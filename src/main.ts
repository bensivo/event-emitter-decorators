import 'reflect-metadata';
import EventEmitter from "events";
import { EventController, EventListener, getEventListeners } from "./event-listener";

@EventController()
export class FooBarController {
    constructor(private emitter: EventEmitter) { }

    @EventListener('foo')
    async onFoo(payload: number) {
        console.log('foo', payload);
        await new Promise((resolve) => setTimeout(resolve, 1000));
        this.emitter.emit('bar', payload * 4)
    }

    @EventListener('bar')
    async onBar(payload: number) {
        console.log('bar', payload);
        await new Promise((resolve) => setTimeout(resolve, 1000));
        this.emitter.emit('foo', payload / 2)
    }
}

const emitter = new EventEmitter();

new FooBarController(emitter);

for (const [eventName, listeners] of Object.entries(getEventListeners())) {
    for(const listener of listeners) {
        emitter.addListener(eventName, listener);
    }
}
emitter.emit('foo', 1)