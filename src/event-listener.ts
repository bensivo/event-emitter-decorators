export function EventListener(eventName: string | symbol) {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        console.log(`Found listener for event '${String(eventName)}': ${target.constructor.name}.${propertyKey}`);
        const eventListeners = Reflect.getMetadata('eventListeners', target) ?? [];
        eventListeners.push({
            propertyKey,
            descriptor,
            eventName
        });
        Reflect.defineMetadata('eventListeners', eventListeners, target);
    };
}

export function EventController() {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {

        return class extends constructor {
            constructor(...args: any[]) {
                super(...args);

                const metadata = Reflect.getMetadata('eventListeners', this);
                for(const data of metadata) {
                    console.log(`Registering event listener ${data.eventName} - ${constructor.name}.${data.propertyKey}`);
                    registerEventListener(data.eventName, data.descriptor.value.bind(this)) ;
                }
            }
        }
    }
}

type EventListenerFunc = (...args: []) => void;

const eventListeners: Record<string, EventListenerFunc[]> = {};

export function registerEventListener(eventName: string, listener: EventListenerFunc) {
    const listeners: EventListenerFunc[] = eventListeners[eventName] ?? [];
    listeners.push(listener);
    eventListeners[eventName] = listeners;
}

export function getEventListeners(): Record<string, EventListenerFunc[]> {
    return eventListeners;
}
